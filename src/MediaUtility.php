<?php

namespace Drupal\media_utility;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Image\ImageFactory;

/**
 * A media utility service for fetching media data.
 */
class MediaUtility {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\File\FileUrlGeneratorInterface definition.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Drupal\Core\File\ImageFactory definition.
   *
   * @var \Drupal\Core\File\ImageFactory
   */
  protected $imageFactory;

  /**
   * Initialize data members.
   */
  public function __construct(EntityTypeManager $entity_manager, FileUrlGeneratorInterface $url_generator, ImageFactory $image_factory) {
    $this->entityTypeManager = $entity_manager;
    $this->urlGenerator = $url_generator;
    $this->imageFactory = $image_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
      $container->get('image.factory'),
    );
  }

  /**
   * Get media type - Image from paragraph`s field.
   */
  public function getMediaImageFromParagraph($paragraph, $field) {
    $image = [];
    if ($paragraph && $field) {
      if ($image_media = $this->entityTypeManager->getStorage('media')->load($paragraph->$field->target_id)) {
        $image_fid = $image_media->field_media_image->target_id ?? '';
        $image_file = $this->entityTypeManager->getStorage('file')->load($image_fid);
        if (\is_object($image_file)) {
          $image_url = $this->urlGenerator->generateAbsoluteString($image_file->uri->value);
          $image = $this->imageFactory->get($image_file->uri->value);
          $width = $image->getWidth() ?? '';
          $height = $image->getHeight() ?? '';
          $media_image_expire = $image_media->field_license_expiry_date->value ?? '';
          if (!empty($media_image_expire)) {
            $media_expire_time = strtotime($media_image_expire);
            $media_expire_time = (string) $media_expire_time;
          }
          $image = [
            'width' => $width ?? '',
            'height' => $height ?? '',
            'url' => $image_url ?? '',
            'alt' => $image_media->field_media_image->alt ?? '',
            'changed' => $image_file->changed->value,
            'expires' => $media_expire_time ?? '',
          ];
        }
      }
    }
    return $image;
  }

  /**
   * Get media type - Document from paragraph`s field.
   */
  public function getMediaDocumentFromParagraph($paragraph, $field) {
    $document = [];
    if ($paragraph && $field) {
      if ($document_media = $this->entityTypeManager->getStorage('media')->load($paragraph->$field->target_id)) {
        $document_fid = $document_media->field_media_document->target_id ?? '';
        $document_file = $this->entityTypeManager->getStorage('file')->load($document_fid);
        if (\is_object($document_file)) {
          $document_url = $this->urlGenerator->generateAbsoluteString($document_file->uri->value);
          $filesize = format_size($document_file->filesize->value);
          $document = [
            'url' => $document_url ?? '',
            'size' => $filesize,
            'changed' => $document_file->changed->value,
          ];
        }
      }
    }
    return $document;
  }

  /**
   * Get media type - video from paragraph`s field.
   */
  public function getMediaRemoteVideoFromParagraph($paragraph, $field) {
    $video_url = '';
    if ($paragraph && $field) {
      $video_media = $this->entityTypeManager->getStorage('media')->load($paragraph->$field->target_id);
      $video_url = $video_media->field_media_oembed_video->value ?? '';
    }
    return $video_url;
  }

  /**
   * Get media type - image.
   */
  public function getMediaImage($mid) {
    $image = [];
    if (empty($mid)) {
      return $image;
    }
    $mid = trim($mid);
    $mid = (int) $mid;
    $image_media = $this->entityTypeManager->getStorage('media')->load($mid);
    $image_fid = $image_media->field_media_image->target_id ?? '';
    if (empty($image_fid)) {
      return $image;
    }
    $image_file = $this->entityTypeManager->getStorage('file')->load($image_fid);
    if (\is_object($image_file)) {
      $image_url = $this->urlGenerator->generateAbsoluteString($image_file->uri->value);
      $image = $this->imageFactory->get($image_file->uri->value);
      $width = $image->getWidth() ?? '';
      $height = $image->getHeight() ?? '';
      $media_image_expire = $image_media->field_license_expiry_date->value ?? '';
      if (!empty($media_image_expire)) {
        $media_expire_time = strtotime($media_image_expire);
        $media_expire_time = (string) $media_expire_time;
      }
      $image = [
        'width' => $width ?? '',
        'height' => $height ?? '',
        'url' => $image_url ?? '',
        'alt' => $image_media->field_media_image->alt ?? '',
        'changed' => $image_file->changed->value,
      ];
    }
    return $image;
  }

  /**
   * Get media type - document.
   */
  public function getMediaDocument($mid) {
    $document = [];
    if (empty($mid)) {
      return $document;
    }
    $mid = trim($mid);
    $mid = (int) $mid;
    $document_media = $this->entityTypeManager->getStorage('media')->load($mid);
    $document_fid = $document_media->field_media_document->target_id ?? '';
    if (empty($document_fid)) {
      return $document;
    }
    else {
      $document_file = $this->entityTypeManager->getStorage('file')->load($document_fid);
      if (\is_object($document_file)) {
        $document_url = $this->urlGenerator->generateAbsoluteString($document_file->uri->value);
        $filesize = format_size($document_file->filesize->value)->render();
        $document = [
          'url' => $document_url ?? '',
          'size' => $filesize,
          'changed' => $document_file->changed->value,
        ];
      }
    }
    return $document;
  }

  /**
   * Get media type - remote video.
   */
  public function getMediaRemoteVideo($mid) {
    $remote_video = '';
    if (empty($mid)) {
      return $remote_video;
    }
    $mid = trim($mid);
    $mid = (int) $mid;
    $remote_video_media = $this->entityTypeManager->getStorage('media')->load($mid);
    $remote_video = $remote_video_media->field_media_oembed_video->value ?? '';
    return $remote_video;
  }

  /**
   * Get media type - audio & video.
   */
  public function getMediaAudioVideo($type, $mid) {
    $audio_video = '';
    if (empty($type) && ($type != 'audio' || $type != 'video')) {
      return $audio_video;
    }
    if (empty($mid)) {
      return $audio_video;
    }
    $mid = trim($mid);
    $mid = (int) $mid;
    $audio_video_media = $this->entityTypeManager->getStorage('media')->load($mid);
    $media_field = 'field_media_' . $type . '_file';
    $audio_video_fid = $audio_video_media->{$media_field}->target_id ?? '';
    if (empty($audio_video_fid)) {
      return $audio_video;
    }
    else {
      $audio_video_file = $this->entityTypeManager->getStorage('file')->load($audio_video_fid);
      if (\is_object($audio_video_file)) {
        $video_url = $this->urlGenerator->generateAbsoluteString($audio_video_file->uri->value);
        $filesize = format_size($audio_video_file->filesize->value)->render();
        $video = [
          'url' => $video_url ?? '',
          'size' => $filesize,
          'changed' => $audio_video_file->changed->value,
        ];
      }
    }
    return $video;
  }

}
