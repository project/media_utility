Please make sure this module has been downloaded using composer.

How to install this module?
---------------------------
1. Install this module by drush command - drush en media_utility
2. Or install by "Extend" menu. (Drupal Admin Backend)


About
-----

The module provides a media utility service.
This service contains some important functions to provide data of
image, document, audio, video and remote video.

The functions can be used in custom module and can save development time.

*In future as per the requests of users, functionality can be enhanced.*
